#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <condition_variable>

#include "myTerm.h"
#include "myBigChars.h"
#include "myReadKey.h"

using namespace std;
bool reciveing = true;
string command, recived_message, chat;
bool wanna_send = false;
bool my_turn = false;
vector<pair<bool, string>> chat_history;
int socketfd;
struct sockaddr_in addr;
string serverip = "127.0.0.1";
int port = 8303;
bool cross = false;
bool game_over = false;
bool win = true;


std::mutex draw_mutex;
condition_variable redraw;


struct board {
    char deck[3][3];
    int xsel, ysel;
};

void printBoard(struct board b) {
    mt_clrscr();
    mt_setbgcolor(0);
    mt_setfgcolor(1);
    int i, j;
    bc_box(1, 1, 27, 27);

    for (i = 0; i < 3; ++i){
        for (j = 0; j < 3; ++j){
            if (i == b.xsel && j == b.ysel) {
                mt_setbgcolor(my_turn?2:4);
            }

            switch(b.deck[i][j]) {
                case 'x':
                    bc_printbigchar(BigChar[18], i * 8 + 2 , j * 8 + 2);
                break;

                case '0':
                    bc_printbigchar(BigChar[0], i * 8 + 2, j * 8 + 2);
                break;

                default:
                    bc_printbigchar(0, i * 8 + 2, j * 8 + 2);
                break;
            };

            if (i == b.xsel && j == b.ysel) {
                mt_setbgcolor(0);
            }
        }
    }

    //mt_gotoXY(2, 30); cout << command;
    //mt_gotoXY(3, 30); cout << recived_message;
    //mt_gotoXY(4, 30); cout << "cross: " << cross;
    mt_gotoXY(2, 30); cout << "your turn: " << my_turn;
    if (chat_history.size()) {
        int chat_item = chat_history.size() - 1;
        for (i = 3; i < 15 && chat_item >= 0 ; ++i){
            mt_gotoXY(i, 30);
            mt_setfgcolor(chat_history[chat_item].first?2:1);
            cout << chat_history[chat_item--].second << '\n';
        }
    }
    cout << endl;
}

void select_cell(struct board *b) {
    mt_cursorVisible(0);

    rk_mytermsave();
    rk_mytermregime(0, 0, 1, 0, 1);
    keys k;

    bool cont = true;
    while(cont && !game_over) {
        rk_readkey(k);

        string for_chat;
        switch(k) {
            case 9: // down
                b->xsel--;
                if (b->xsel < 0) {
                    b->xsel = 2;
                }
            break;

            case 1: // l
                mt_gotoXY(1, 30);
                //cout << recived_message; 
                printf("%s", recived_message.c_str());
            break;

            case 2: // s
                mt_cursorVisible(1);
                rk_mytermregime(0, 0, 1, 1, 1);
                mt_gotoXY(1, 30);
                cout << "say:";
                getline(cin, for_chat);
                chat_history.push_back(pair<bool, string>(true, for_chat));
                command.clear();
                command = "chat";
                command.append(for_chat);

                rk_mytermregime(0, 0, 1, 0, 1);
                mt_cursorVisible(0);
                wanna_send = true;
                redraw.notify_one();
                continue;
            break;

            case 8: // up
                b->xsel++;
                if (b->xsel > 2) {
                    b->xsel = 0;
                }
            break;

            case 10: // left
                b->ysel--;
                if (b->ysel < 0) {
                    b->ysel = 2;
                }
            break;

            case 11: // right
                b->ysel++;
                if (b->ysel > 2) {
                    b->ysel = 0;
                }
            break;

            case 12: // enter
                if (my_turn) {
                    my_turn = false;
                    command = "ll";
                    if (b->deck[b->xsel][b->ysel] == 'x' || b->deck[b->xsel][b->ysel] == '0') {
                        my_turn = true;
                        unique_lock<std::mutex> lock(draw_mutex);
                        redraw.notify_one();
                        continue;
                    }
                    b->deck[b->xsel][b->ysel] = cross?'x':'0';
                    command[0] = b->xsel + '0';
                    command[1] = b->ysel + '0';
                    wanna_send = true;

                    unique_lock<std::mutex> lock(draw_mutex);
                    redraw.notify_one();
                }
                continue;
            break;

            default:
                unique_lock<std::mutex> lock(draw_mutex);
                redraw.notify_one();
                continue;
            break;
        };
        unique_lock<std::mutex> lock(draw_mutex);
        redraw.notify_one();
    }
    rk_mytermload();
    mt_cursorVisible(1);
}

void accepting_messages(int socketfd, struct board *b)
{
    reciveing = true;
    cout << "Connection esatablished, press any key to play..." << endl;
    fd_set readset;

    while(!game_over) {

        FD_ZERO(&readset);
        FD_SET(socketfd, &readset);

        timeval t;
        t.tv_sec = 0;
        t.tv_usec = 100000;

        if (wanna_send) {
            int bytes_sent;
            bytes_sent = send(socketfd, command.c_str(), command.length(), 0);
            wanna_send = false;
        }

        if(select(socketfd + 1, &readset, NULL, NULL, &t) < 0) {
            perror("t::select");
            return;
        }


        if (FD_ISSET(socketfd, &readset)) {

            //reciving data
            ssize_t bytes_recived;
            char incoming_buffer[1024];
            bytes_recived = recv(socketfd, incoming_buffer, sizeof(incoming_buffer), 0);
            if (bytes_recived > 0) {
                incoming_buffer[bytes_recived] = '\0';
            } else {
                break;
            }

            recived_message = incoming_buffer;
            if (recived_message == "win") {
                game_over = true;
                win = true;
            }
            if (recived_message == "loose") {
                game_over = true;
                win = false;
            }
            if (recived_message == "first_turn") {
                cross = true;
                my_turn = true;
            }
            if (recived_message.find("chat", 0) == 0) {
                chat = &recived_message.c_str()[4];
                chat_history.push_back(pair<bool, string>(false, chat));
            }

            if (recived_message.size() == 2) {
                int x, y;
                x = recived_message[0] - '0';
                y = recived_message[1] - '0';
                b->deck[x][y] = cross?'0':'x';
                //} else if(recived_message == "turn"){
                my_turn = true;
            }
            unique_lock<std::mutex> lock(draw_mutex);
            redraw.notify_one();
        }

    }
    mt_gotoXY(1, 30); cout << "NETWORK FAILED!" << endl;
    reciveing = false;
}

void printBoard_loop(struct board *b){
    while(!game_over) {
        unique_lock<std::mutex> lock(draw_mutex);
        redraw.wait(lock);
        printBoard(*b);
    }
}

void parallel() {
    fcntl(socketfd, F_SETFL, O_NONBLOCK);

    struct board b;
    int i, j;
    for (i = 0; i < 3; ++i){
        for (j = 0; j < 3; ++j){
            b.deck[i][j] = ' ';
        }
    }
    b.xsel = 1;
    b.ysel = 1;

    thread servmessages(accepting_messages, socketfd, &b);
    thread interface(printBoard_loop, &b);

    select_cell(&b);

    servmessages.join();
    interface.join();

}

string request(string message) {
    int bytes_sent, bytes_recived;
    char buf[1024];
    bytes_sent = send(socketfd, message.c_str(), message.length(), 0);
    bytes_recived = recv(socketfd, buf, sizeof(buf), 0);
    buf[bytes_recived] = '\0';
    string answer(buf);
    return answer;
}

void command_line() {
    cout << "entering command line" << endl;
    do {
        cin >> command;
        cout << request(command) << endl;
    } while(1);
}

void succesively() {
    cout << "create(1) or join(2)?" << endl;
    cin >> command;

    string server_message, answer;
    if (command == "1") {
        answer = request("create");
        cout << "'" << answer << "'" << endl;
        if (answer == "enter_name") {
            cout << "Enter name for this game:";
            cin >> server_message;
            answer = request(server_message);
            if (answer == "created") {
                parallel();
            } else {
                cout << "Game not created..." << endl;
                return;
            }

        } else {
            cout << "You cant create game, because server reject it" << endl;
            return;
        }

    } else if (command == "2") {
        answer = request("join");
        if (answer == "no_games") {
            cout << "No created games, sorry..." << endl;
            return;
        } else {
            cout << answer;
            cin >> server_message;
            answer = request(server_message);
            if (answer == "not_found") {
                cout << "Game not found" << endl;
            } else {
                parallel();
            }
        }

    } else {
        cout << "wrong command.. " << endl;
        return;
    }
}

int main(int argc, char **argv)
{

    // arguments processing
    string current_argument;
    if (argc > 1)
        for (int i = 1; argv[i] != NULL; i++ ){
            current_argument = argv[i];
            if (argv[i+1] == NULL) {
                cout << "missing parametr" << endl;
            }
            if ((current_argument.compare("-s") == 0) || (current_argument.compare("--server") == 0)) {
                serverip = argv[i+1];
                continue;
            }
            if ((current_argument.compare("-p") == 0) || (current_argument.compare("--port") == 0)) {
                port = atoi(argv[i+1]);
                continue;
            }
        }
    cout << serverip << " " << port << endl;

    // creating socket for ipv4
    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd < 0) {
        cerr << "socket creating error" << endl;
        _exit(1);
    }

    // ip address with our port and serverip
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(serverip.c_str());
    if (connect(socketfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        cerr << "connect error" << endl;
        _exit(2);
    }

    //parallel();
    succesively();
    //command_line();
    unsigned long long cong[5];
    
    if (game_over) {
        mt_clrscr();
        mt_setfgcolor(2);
        if (win) {
            cong[0] = 0;
            cong[1] = BigChar[1];
            cong[2] = BigChar[2];
            cong[3] = BigChar[3];
            cong[4] = 0;
        } else {

            cong[0] = BigChar[6];
            cong[1] = BigChar[0];
            cong[2] = BigChar[0];
            cong[3] = BigChar[5];
            cong[4] = BigChar[16];
        }

        int i;
        for (i = 0; i < 5; ++i){
            bc_printbigchar(cong[i], 2, i * 8 + 1);
            cout << endl;
        }
        bc_box(1, 1, 42, 11);
        cout << endl;
    }
    return 0;
}
