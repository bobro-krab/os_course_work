COMPILER=g++
CFLAGS= -g -Wall -pthread -std=c++11
SOURSES=client.cpp
OBJECTS=$(SOURSES:.cpp=.o)
EXECUTABLE=client

all: $(SOURSES) $(EXECUTABLE) server

$(EXECUTABLE): $(OBJECTS)
	$(COMPILER) $(CFLAGS) $(OBJECTS) -o $@

$(OBJECTS): $(SOURSES)
	$(COMPILER) $(CFLAGS) -c $(SOURSES)

clean:
	rm $(OBJECTS) $(EXECUTABLE) server

server: FORCE
	$(COMPILER) $(CFLAGS) ./server.cpp -o server -pthread

FORCE:

run: all
	./$(EXECUTABLE)
