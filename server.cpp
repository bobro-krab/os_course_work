#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <set>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std;
enum client_state {st_connected, st_create, st_join, st_playing};
string state_descriptions[] = {"Connected", "Create", "Join", "Playing"};

class cclient
{
    public:
    int socket;
    enum client_state state;
    bool taken;
};

class game
{
    public:
    int client_created;
    int client_joined;
    string name;
};

string request(int socketfd, string message) {
    int bytes_sent, bytes_recived;
    char buf[1024];
    bytes_sent = send(socketfd, message.c_str(), message.length(), 0);
    bytes_recived = recv(socketfd, buf, sizeof(buf), 0);
    if (bytes_recived <= 0) {
        string answer;
        answer.clear();
        return answer;
    }
    buf[bytes_recived] = '\0';
    string answer(buf);
    return answer;
}

char check_winner(char deck[3][3]) 
{
  int i;
  int j;
  int count = 0;
  for (i = 0; i < 3; ++i){
      for (j = 0; j < 3; ++j){
          if (deck[i][j] == 'x' || deck[i][j] == '0') {
              count++;
          }
      }
  }
  if (count == 9) {
      return 'n';
  }
  for(i=0; i<3; i++)  /* проверка строк */
    if(deck[i][0]==deck[i][1] &&
       deck[i][0]==deck[i][2]) return deck[i][0];

  for(i=0; i<3; i++)  /* проверка столбцов */
    if(deck[0][i]==deck[1][i] &&
       deck[0][i]==deck[2][i]) return deck[0][i];

  /* проверка диагоналей */
  if(deck[0][0]==deck[1][1] &&
     deck[1][1]==deck[2][2])
       return deck[0][0];

  if(deck[0][2]==deck[1][1] &&
     deck[1][1]==deck[2][0])
       return deck[0][2];

  return '.';
}

string recive_string(int socket)
{
    int bytes_read = 0;
    char buf[1024];
    bytes_read = recv(socket, buf, sizeof(buf), 0);
    if (bytes_read == 0) {
        return "";
    }
    buf[bytes_read] = '\0';
    string res(buf);
    return res;
}

void send_string(int socket, string message)
{
    send(socket, message.c_str(), message.size(), 0);
}

void process_game(class game arg)
{
    cout << arg.name << " : " << arg.client_joined << " " << arg.client_created << endl;

    vector<int> sockets;
    sockets.push_back(arg.client_created);
    sockets.push_back(arg.client_joined);

    string client_message, answer;
//    int turn = rand() % 2;
    int turn = 0;
    char deck[3][3];
    bool game_done = false;

    int i, j;
    for (i = 0; i < 3; ++i){
        for (j = 0; j < 3; ++j){
            deck[i][j] = '.';
        }
    }

    client_message = "first_turn";
    send_string(sockets[turn], client_message);

    int winner, looser;
    bool error = false;
    while(!game_done) {
        // Заполняем множество сокетов
        fd_set readset;
        FD_ZERO(&readset);

        int max_socket = -1;
        for(vector<int>::iterator it = sockets.begin(); it != sockets.end(); it++)
        {
            // skip listening thats playing already
            FD_SET(*it, &readset);
            max_socket = max(max_socket, *it);
        }

        // Ждём события в одном из сокетов
        if(select(max_socket + 1, &readset, NULL, NULL, NULL) <= 0)
        {
            perror("select");
            continue;
        }

        char sign[] = {'x', '0'};

        for (i = 0; i < sockets.size(); ++i){
            if(FD_ISSET(sockets[i], &readset))
            {
                answer = recive_string(sockets[i]);
                if (answer.size() == 0) {
                    game_done = true;
                    error = true;
                }
                if (answer.find("chat", 0) == 0) {
                    send_string(sockets[(i+1)%2], answer);
                }
                if (answer.size() == 2 && turn == i) {

                    int x, y;
                    // get turn
                    x = answer[0] - '0';
                    y = answer[1] - '0';
                    cout << sockets[turn] << " '" << client_message << "' '" << x << " " << y << "'" <<  endl;

                    // and save it to table
                    deck[x][y] = sign[turn];
                    for (i = 0; i < 3; ++i){
                        for (j = 0; j < 3; ++j){
                            cout << deck[i][j];
                        }
                        cout << endl;
                    }

                    // rememmber last move
                    client_message = answer;

                    if (check_winner(deck) != '.') {
                        if (check_winner(deck) == 'x') {
                            winner = 0;
                            looser = 1;
                        } else if(check_winner(deck) == '0'){
                            winner = 1;
                            looser = 0;
                        } else {
                            winner = 0;
                            looser = 0;
                        }
                        game_done = true;
                        break;
                    }

                    turn = (turn + 1) % 2;
                    //send_string(sockets[turn], "turn");
                    send_string(sockets[turn], client_message);
                }
            }
        }
    }

    if (error) {
        cout << arg.name << " thread done with errors" << endl;
    } else {
        if (winner == looser) {
            cout << "no one win" << endl;
            send(sockets[0], "win", sizeof("win"), 0);
            send(sockets[1], "win", sizeof("win"), 0);
        } else {
            cout << "game done " << sockets[winner] << " is winner" << endl;
            send(sockets[winner], "win", sizeof("win"), 0);
            send(sockets[looser], "loose", sizeof("loose"), 0);
        }
    }

    shutdown(sockets[0], SHUT_RDWR);
    close(sockets[0]);
    cout << "client " << sockets[0] << " disconnected" << endl;

    shutdown(sockets[1], SHUT_RDWR);
    close(sockets[1]);
    cout << "client " << sockets[1] << " disconnected" << endl;
}

vector<game> games;
vector<thread *> threads;
int main(int argc, char **argv)
{

    int port = 8303;
    int game_count = 5;

    // arguments processing
    string current_argument;
    if (argc > 1)
        for (int i = 1; argv[i] != NULL; i++ ){
            current_argument = argv[i];
            if (argv[i+1] == NULL) {
                cout << "missing parametr" << endl;
            }
            if ((current_argument.compare("-g") == 0) || (current_argument.compare("--game-count") == 0)) {
                game_count = atoi(argv[i+1]);
                continue;
            }
            if ((current_argument.compare("-p") == 0) || (current_argument.compare("--port") == 0)) {
                port = atoi(argv[i+1]);
                continue;
            }
        }

    cout << "server running..." << endl;
    cout << "port: " << port << endl;
    cout << "game_count: " << game_count << endl;

    int listener;
    struct sockaddr_in addr;
    char buf[1024];
    int bytes_read;

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }

    int yes = 1;
    if ( setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
    {
        perror("setsockopt");
    }

    fcntl(listener, F_SETFL, O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, 2);

    vector<cclient> clients;
    clients.clear();

    while(1)
    {
        // Заполняем множество сокетов
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(listener, &readset);

        int max_socket = -1;
        for(vector<cclient>::iterator it = clients.begin(); it != clients.end(); it++)
        {
            // skip listening thats playing already
            if (it->state == st_playing) {
                continue;
            }
            FD_SET(it->socket, &readset);
            max_socket = max(max_socket, it->socket);
        }

        // Задаём таймаут
        // timeval timeout;
        // timeout.tv_sec = 60;
        // timeout.tv_usec = 0;

        // Ждём события в одном из сокетов
        int mx = max(listener, max_socket);
        if(select(mx + 1, &readset, NULL, NULL, NULL) <= 0)
        {
            perror("select");
            //exit(3);
            continue;
        }

        // Определяем тип события и выполняем соответствующие действия
        if(FD_ISSET(listener, &readset))
        {
            // Поступил новый запрос на соединение, используем accept
            int sock = accept(listener, NULL, NULL);
            if(sock < 0)
            {
                perror("accept");
                continue;
            }

            fcntl(sock, F_SETFL, O_NONBLOCK);

            cclient c;
            c.state = st_connected;
            c.socket = sock;
            c.taken = false;
            clients.push_back(c);
            cout << "client " << sock << " connected\n";
        }

        bool continue_for = true;
        for(vector<cclient>::iterator it = clients.begin(); it != clients.end() && continue_for; it++)
        {
            cout << "games(" << games.size() << ")";
            for (int i = 0; i < games.size(); ++i){
                cout << "[" << games[i].name << " ";
                cout << games[i].client_joined << " ";
                cout << games[i].client_created << "]";
            }
            cout << endl;

            if (it == clients.end()) {
                break;
            }

            if(FD_ISSET(it->socket, &readset))
            {
                // Поступили данные от клиента, читаем их
                bytes_read = recv(it->socket, buf, 1024, 0);

                if(bytes_read <= 0)
                {
                    // Соединение разорвано, удаляем сокет из множества
                    close(it->socket);
                    clients.erase(it);
                    cout << "client " << it->socket << " disconnected" << endl;
                    continue;
                }
                buf[bytes_read] = '\0';

                string question(buf), answer(buf);
                cout << it->socket << ">>" << question << endl;

                game g;
                bool found = false;

                if (question == "state") {
                    answer = state_descriptions[it->state];
                    send(it->socket, answer.c_str(), answer.size(), 0);
                    continue;
                }

                switch(it->state){
                    case st_connected:
                        if (question == "create") {
                            answer = "enter_name";
                            it->state = st_create;
                        }
                        if (question == "join") {
                            answer.clear();
                            int i;
                            for (i = 0; i < games.size(); ++i){
                                answer.append(games[i].name + "\n");
                            }
                            if (!answer.size()) {
                                answer = "no_games";
                            }
                            it->state = st_join;
                        }
                    break; //st_connected

                    case st_create:
                        if (games.size() < game_count) {
                            g.name = question;
                            g.client_created = it->socket;
                            games.push_back(g);
                            answer = "created";
                        } else {
                            answer = "not_created";
                        }
                        it->state = st_playing;
                    break; // st_create

                    case st_join:
                        for(vector<game>::iterator current_game = games.begin();
                                current_game != games.end();
                                current_game++)
                        {
                            if (current_game->name == question) {
                                found = true;
                                current_game->client_joined = it->socket;

                                thread *t = new thread(process_game, *current_game);
                                threads.push_back(t);

                                it->state = st_playing;

                                FD_CLR(current_game->client_created, &readset);
                                FD_CLR(current_game->client_joined, &readset);
                                games.erase(current_game--);
                                break;
                            }
                        }
                        if (!found) {
                            it->state = st_connected;
                            answer = "not_found";
                        } else {
                            answer = "joined";
                        }
                    break; //st_join

                    case st_playing:
                        clients.erase(it--);
                        continue;
                    break; // st_playing
                }

                send(it->socket, answer.c_str(), answer.size(), 0);
            }
        }
    }

    for (int i = 0; i < threads.size(); ++i){
        threads[i]->join();
    }
    return 0;
}
