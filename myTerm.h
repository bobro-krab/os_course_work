#ifndef MYTERM_H_
#define MYTERM_H_

#include <termios.h>
#include <sys/ioctl.h>
#include <cstdio>

//отчистить экран
int mt_clrscr(void)
{
    printf("\E[H\E[2J");
    return 0;
}

//переместить курсор
int mt_gotoXY(int x, int y)
{
    if(x < 0 || y < 0) 
        return 1;
    printf("\E[%d;%dH", x, y);
    return 0;
}

//определение размера экрана
int mt_getscreensize(int *rows,int *cols)
{
    struct winsize ws;

    if (!ioctl(1, TIOCGWINSZ, &ws))
    {
         *rows = ws.ws_row;
         *cols = ws.ws_col;
         return 0;
    }
    else 
        return 1;
}

//установить цвет фона символов
int mt_setbgcolor(int colors)
{
    if(colors >= 0 && colors < 8)
    {
        printf("\E[4%dm",colors);
        return 0;
    }
    else 
        return 1;
}

//установить цвет символов
int mt_setfgcolor(int colors)
{
    if(colors >= 0 && colors < 8)
    {
        printf("\E[3%dm", colors);
        return 0;
    }
    else 
        return 1;
}

#endif
