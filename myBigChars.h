#ifndef MYBC
#define MYBC 
#include <stdio.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/time.h>

enum colors 
{
    BLACK, RED, GREEN, YELLOW, 
    BLUE, VIOLET, LIGHT_BLUE, WHITE
};

#include "myTerm.h"

unsigned long long BigChar[20] =
{
    //0x7EFFE7EFF7E7FF7E, // 0 0
    0x3c66424a5242663c, // 0 0
    0x24247e5adbc38181, // w
    0x3c1818181818183c, // i
    0xc3e3f3dbcfc7c3c3, // n
    0xfe67037ffec0e77e, // s
    0x7FE0E0FE7F0707FF, // 5 5
    0xe7e7060606060606, // l
    0xE0E0E0E0E0E0E07F, // 7
    0x7EFFE77E7EE7FF7E, // 8
    0x7FE0E0FEE7E7FF7E, // 9
    0x3C3C3CFFFF3C3C3C, // + 10
    0x000000FFFF000000, // -
    0x8141427e24241810, //A 10
    0x3f4141413f41413f, //B 11
    0x3c4201010101423c, //C 12
    0x3e4282828282423e, //D 13 15
    0x3f0101013f01013f, //E 14
    0x10101010f01013f,  //F 15
    0x8142241818244281, // X 18
    280379743272960  // = 19
};


int bc_printA (const char *str)
{
  if (str != NULL) {
      printf ("\E(0");
      printf ("%s", str);
      printf ("\E(B");
      return 0;
    }
  else return -1;

}

int bc_box (int x1, int y1, int x2, int y2)
{
  int i, ym, xm;
  mt_getscreensize(&xm , &ym);

  for (i = x1; i < x2; i++) {
    mt_gotoXY(y1, i); bc_printA ("q");
    mt_gotoXY(y2 - 1, i); bc_printA ("q");
  }

  for (i = y1; i < y2; i++) {
      mt_gotoXY(i, x1); bc_printA ("x");
      mt_gotoXY(i, x2); bc_printA ("x");
  }

  mt_gotoXY(y1, x1); bc_printA("l");
  mt_gotoXY(y1, x2); bc_printA("k");
  mt_gotoXY(y2 - 1, x1); bc_printA("m");
  mt_gotoXY(y2 - 1, x2); bc_printA("j");

  return 0;
}

int bc_printbigchar (unsigned long long big, int x, int y)
{
  int i, k = 0;

  mt_gotoXY(x++, y);

    for(i = 0; i < 64; i++) {
            if ((big & 1) == 1) bc_printA ("a");
            else bc_printA (" ");
            k++;
            if (k >= 8) {
                mt_gotoXY(x++, y);
                k = 0;
            }
            big = big >> 1;
    }

  return 0;
}

#endif
